﻿#include <iostream>
#include <string_view>
#include <time.h>
using namespace std;


void main()
{
    const int N = 4;
    int b;
    int c = 0;
    int a[N][N];

    struct tm buf;
    time_t t = time(NULL);
    localtime_s(&buf,&t);

    cout << "Data \n" << buf.tm_mday << endl;
    b = buf.tm_mday % N;

    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
        {
            a[i][j] = i + j;
            cout << a[i][j];
        }
        cout << "\n";
    }
    for (int j = 0; j < N; j++)
    {
        c += a[b][j];
    }
    cout << "Sum string  " << b << "  equal to  " << c;
}